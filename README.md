# Toxic comments modelling 

### Description
The data comes from [***kaggle competition***](https://www.kaggle.com/c/jigsaw-toxic-comment-classification-challenge/data). The goal of the project is to classify toxic comments which were collected from Wikipedia. The target is a vector which contains six categories. Several models were used including: Multinomial Naive Bayes, LSTM, Biderectional LSTM, Fully-Connected Neural Network with different vector representations.  

Some ideas are taken from [**Hands-On Machine Learning with Scikit-Learn, Keras, and TensorFlow by A.Geron**](https://www.amazon.com/Hands-Machine-Learning-Scikit-Learn-TensorFlow/dp/1492032646/ref=sr_1_1?crid=21LH15LZLF6RW&keywords=Hands-On+Machine+Learning+with+Scikit-Learn%2C+Keras&qid=1661703127&sprefix=hands-on+machine+learning+with+scikit-learn%2C+keras%2Caps%2C69&sr=8-1) book.
